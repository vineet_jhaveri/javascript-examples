
var cars = ["BMW", "Skoda", "Audi", "Tata", "Maruti"];
var bikes = ["Yamaha", "Honda", "Hero", "Bajaj", "Ducati"];
var res = ["BMW", "Skoda", "Audi", "Tata", "Maruti"];
var rtn = "";

function arrLoad() {
	document.getElementById("cars").innerHTML = cars;
	document.getElementById("bikes").innerHTML = bikes;
	// document.getElementById("output").innerHTML = cars;
}

function resetIt() {
	res = cars.slice();
	document.getElementById("output").innerHTML = "";
	rtn = "";
	document.getElementById("return").innerHTML = rtn;
}

function outputArr() {
	document.getElementById("output").innerHTML = res;
	document.getElementById("return").innerHTML = rtn;
}

function popIt() {
	rtn = res.pop();
	outputArr();
}

function pushIt() {
	var x = document.getElementById("push").value;
	rtn = res.push(x);
	outputArr();
}

function shiftIt() {
	rtn = res.shift();
	outputArr();
}

function unshiftIt() {
	var x = document.getElementById("unshift").value;
	rtn = res.unshift(x);
	outputArr();
}

function spliceIt() {
	var x = document.getElementById("spPos").value;
	var y = document.getElementById("spCount").value;
	var z = document.getElementById("spVal").value;

	// var x = 1;
	// var y = 2;
	// var z = "kwid";

	rtn = res.splice(x , y, z);
	outputArr();
}

function sortIt() {
	res.sort();
	rtn = "";
	outputArr();
}

function reverseIt() {
	res.reverse();
	rtn = "";
	outputArr();
}

function concatIt() {
	rtn = cars.concat(bikes);
	outputArr();
}

function sliceIt() {
	var x = document.getElementById("slStart").value;
	var y = document.getElementById("slEnd").value;

	// var x = 2;
	// var y = 2;

	rtn = res.slice(x, y);
	outputArr();
}
